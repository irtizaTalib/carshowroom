public class Model {

    private String modelNumber;
    private String enginePower;
    private String tyreSize;
    private String engineType;
    private String vehicleType;
    private String specialType;

    public Model() {
    }

    public Model(String modelNumberParam, String vehicleTypeParam, String enginePowerParam, String tyreSizeParam, String engineTypeParam) {

        this.modelNumber = modelNumberParam;
        this.vehicleType = vehicleTypeParam;
        this.enginePower = enginePowerParam;
        this.tyreSize = tyreSizeParam;
        this.engineType = engineTypeParam;
        this.specialType = "none";
    }
    public Model(String modelNumberParam, String vehicleTypeParam, String enginePowerParam, String tyreSizeParam) {

        this.modelNumber = modelNumberParam;
        this.vehicleType = vehicleTypeParam;
        this.enginePower = enginePowerParam;
        this.tyreSize = tyreSizeParam;
        if(this.vehicleType == "sports" || this.vehicleType =="Sports"){
            this.engineType = "oil";
            this.specialType = "turbo";
        }
        else{
            this.engineType = "diesel";
            this.specialType = "weight";
        }
    }

    // SET METHOD

    public void setModelNumber(String a) {
        this.modelNumber = a;
    }

    public void setVehicleType(String a) {
        this.vehicleType = a;
    }

    public void setEnginePower(String a) {
        this.enginePower = a;
    }

    public void setTyreSize(String a) {
        this.tyreSize = a;
    }

    public void setEngineType(String a) {
        this.engineType = a;
    }

    public void setSpecialType(String a) {
        this.specialType = a;
    }


    //GET METHOD

    public String getModelNumber() {
        return this.modelNumber;
    }

    public String getVehicleType() {
        return this.vehicleType;
    }

    public String getEnginePower() {
        return this.enginePower;
    }

    public String getTyreSize() {
        return this.tyreSize;
    }

    public String getEngineType() {
        return this.engineType;
    }

    public String getSpecialType() {
        return this.specialType;
    }

}

