import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;


public class CarShowRoom {


    public static void viewListOfCars(List<Model> data_name) {
        // VIEW ALL CARS INFORMATION
        System.out.println("\nList of all Cars ...");
        int id = 1;
        for (Model model : data_name) {
            System.out.println("car ID: " + id++);
            System.out.println("modelNumber: " + model.getModelNumber());
            System.out.println("vehicleType: " + model.getVehicleType());
            System.out.println("enginePower: " + model.getEnginePower());
            System.out.println("tyreSize: " + model.getTyreSize());
            System.out.println("engineType: " + model.getEngineType());
            System.out.println("specialType: " + model.getSpecialType());
            System.out.println("\n");
        }
    }
    public static int visitorCounter(List<Model> data_name, int visitors) {
        // COUNT VISITORS
        int visitorCount = visitors;
        for (Model model : data_name) {
            if(model.getVehicleType().equals("sports") || model.getVehicleType().equals("Sports") ){
                visitorCount = visitorCount + 20;
            }
        }
        return visitorCount;
    }

    public static void main(String[] args) {
        int visitors = 30;

        Scanner sc = new Scanner(System.in);
        List<Model> data_name = new ArrayList<Model>();

        Model car1 = new Model("towota12", "normal", "2000cc", "12", "gas");
        data_name.add(car1);
        Model car2 = new Model("nissan GTR", "sports", "8000cc", "20");
        data_name.add(car2);
        Model car3 = new Model("lorrry1", "heavy", "10000cc", "40");
        data_name.add(car3);
        Model car4 = new Model("maruti", "normal", "200cc", "5", "oil");
        data_name.add(car4);

        viewListOfCars(data_name);

        System.out.println("Would you like to insert a car or delete, input 1 for inserting and 2 for delete");
        int inputOP = sc.nextInt();

        if (inputOP == 1) {

            System.out.println("please provide the number of cars you would like to input");
            int nInputs = sc.nextInt();

            sc.nextLine();
            for (int i = 0; i < nInputs; i++) {

                Model car = new Model();

                System.out.println("please input the " + i + " car model number:");
                String modelNum = sc.nextLine();

                System.out.println("Please input the vehicle type: ");
                String vehicleType = sc.nextLine().trim();
                System.out.println("vehicle type length :"+ vehicleType.length() + vehicleType);

                System.out.println("Please input the engine power: ");
                String enginePower = sc.nextLine();

                System.out.println("Please input the tyre size: ");
                String tyreSize = sc.nextLine();

                String engineType;
                String specialType;
                if (vehicleType.equals("normal") || vehicleType.equals("Normal") ) {
                    System.out.println("Please input the engine type:");
                    engineType = sc.nextLine();
                    specialType = "none";
                } else {
                    if (vehicleType.equals("sports") || vehicleType.equals("Sports") ) {
                        engineType = "oil";
                        specialType = "turbo";
                    } else {
                        engineType = "diesel";
                        specialType = "weight";
                    }
                }


                car.setModelNumber(modelNum);
                car.setVehicleType(vehicleType);
                car.setEnginePower(enginePower);
                car.setTyreSize(tyreSize);
                car.setEngineType(engineType);
                car.setSpecialType(specialType);
                data_name.add(car);

                System.out.println("\n");
            }
        } else if (inputOP == 2) {

            System.out.println("Please input the car id which you would like to delete");

            int del = sc.nextInt();

            del = del - 1;
            data_name.remove(del);
            System.out.println("Your data has been removed");

        } else {
            System.out.println("Wrong input!!");
        }

        System.out.println("Current expected visitors : " + visitorCounter(data_name, visitors) );
        viewListOfCars(data_name);
    }

}
